<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\SmallProduct;
use AppBundle\Form\SmallProductType;

/**
 * SmallProduct controller.
 *
 */
class SmallProductController extends Controller
{

    /**
     * Lists all SmallProduct entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:SmallProduct')->findAll();

        return $this->render(
            'AppBundle:SmallProduct:index.html.twig',
            array(
                'entities' => $entities,
            )
        );
    }

    /**
     * Creates a new SmallProduct entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SmallProduct();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('small-product_show', array('id' => $entity->getId())));
        }

        return $this->render(
            'AppBundle:SmallProduct:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Creates a form to create a SmallProduct entity.
     *
     * @param SmallProduct $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SmallProduct $entity)
    {
        $form = $this->createForm(
            new SmallProductType(),
            $entity,
            array(
                'action' => $this->generateUrl('small-product_create'),
                'method' => 'POST',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SmallProduct entity.
     *
     */
    public function newAction()
    {
        $entity = new SmallProduct();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'AppBundle:SmallProduct:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a SmallProduct entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:SmallProduct')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SmallProduct entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:SmallProduct:show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing SmallProduct entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:SmallProduct')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SmallProduct entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:SmallProduct:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Creates a form to edit a SmallProduct entity.
     *
     * @param SmallProduct $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(SmallProduct $entity)
    {
        $form = $this->createForm(
            new SmallProductType(),
            $entity,
            array(
                'action' => $this->generateUrl('small-product_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing SmallProduct entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:SmallProduct')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SmallProduct entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('small-product_edit', array('id' => $id)));
        }

        return $this->render(
            'AppBundle:SmallProduct:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a SmallProduct entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:SmallProduct')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SmallProduct entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('small-product'));
    }

    /**
     * Creates a form to delete a SmallProduct entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('small-product_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }
}
