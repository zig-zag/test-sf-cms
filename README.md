Product Admin Panel
===========

CRUD for products' admin panel

# Requirements

1. [Vagrant](https://www.vagrantup.com/)
2. [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
3. [Ansible](http://docs.ansible.com/intro_installation.html)
4. Internet connection

# Installation

1. Run from *{project_root}/etc*

    ```
    $ vagrant up
    ```
    
2. If Vagrant up fails try to run it again 1 more time (Ansible provisioning is not always stable & some connection errors can cause fails)
3. Update */etc/hosts* on your master machine. Add line

    ```
    192.168.33.150 test-sf-cms.vm test-sf-cms
    ```

4. Open [web application](http://test-sf-cms.vm) in your browser and fill in your data


# TODO
- Pagination